# vue-safe-html

A Vue Directive to sanitize html to prevent XSS attacks. Provided HTML code is sanitized using [DOMPurify](https://github.com/cure53/DOMPurify).

### Example

```html
<div v-safe-html="rawHtml"></div>
```

#### Advance configuration
```js
// This will allow only <b> tag
const config = { ALLOWED_TAGS: ['b'] }
```
```html
<div v-safe-html:[config]="rawHtml"></div>
```
Please refer https://github.com/cure53/DOMPurify#can-i-configure-dompurify for more configuration options

### Demo
https://djadmin.gitlab.io/vue-safe-html/