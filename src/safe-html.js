// Strips out dirty HTML and returns clean HTML

import Vue from "vue";
import { sanitize } from "dompurify";

const transform = (el, binding) => {
  if (binding.oldValue === binding.value) {
    return;
  }
  const config = binding.arg;
  el.innerHTML = sanitize(binding.value, config);
};

Vue.directive("safe-html", {
  bind: transform,
  update: transform,
});
